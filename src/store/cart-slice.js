import { createSlice } from "@reduxjs/toolkit";

const cartSlice =createSlice({
    name: 'cart',
    initialState: {
        itemsList: [],
        totalQuantity: 0,
        showCart: false
    },
    reducers:{
        addToCart(state,action) {
            const newItem=action.payload;
            // for more click, increase numner 
            const existingItem = state.itemsList.find((item) => item.id === newItem.id);

            if(existingItem){
                // this product is already selected
                existingItem.quantity++;
                existingItem.totalPrice+= newItem.price
            }else{
                // this is a new product so it should be added to list
                state.itemsList.push({
                    id: newItem.id,
                    price: newItem.price,
                    quantity: 1,
                    totalPrice: newItem.price,
                    name: newItem.name,
                })
                state.totalQuantity++
                // to show the total quantity on the main shoping cart 
            }
         },
        removeFromCart(state,action) {
            const id = action.payload;
            const existingItem = state.itemsList.find(item => item.id ===id);
            // if the quantity is equal to 1, remove the item from the list , else reduce the numbers
            if(existingItem.quantity ===1){
                state.itemsList =state.itemsList.filter(item => item.id !==id);
                state.quantity--;
            } else {
                existingItem.quantity--;
                existingItem.totalPrice -= existingItem.price;
            }
         },
        setShowCart(state) {
            state.showCart = !state.showCart;
         },
    }, 
})

export const cartActions = cartSlice.actions;

export default cartSlice;