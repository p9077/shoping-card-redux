import React from "react";
import "./App.css";
import Auth from "./components/Auth";
import Layout from "./components/Layout";
import { useSelector } from "react-redux";

function App() {
  // ta 1:09 
  const isLoggedIn = useSelector( (state) => state.auth.isLoggedIn);
  console.log(isLoggedIn);
  const cartItems = useSelector((state) => state.cart.itemsList);
  console.log(cartItems);
  return (
    <div className="App">
    {/* based on login or logout, one of these pages will be shown in front page */}
    {/* if the user is logged in => show Layot. Otherwise => show Auth */}
      {!isLoggedIn && <Auth />}
      {isLoggedIn && <Layout />}
      {/* <Layout /> */}
    </div>
  );
}

export default App;
